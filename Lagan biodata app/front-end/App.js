import * as React from 'react';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import authReducer from './src/store/reducers/authReducer';
import profileReducer from './src/store/reducers/profileReducer';
import { AppNavigation } from './src/screens/AppNavigation';
import ReduxThunk from 'redux-thunk';

const rootReducer = combineReducers({
  auth: authReducer,
  profile: profileReducer,
});

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));


export default function App() {

  return (
    <Provider store={store}>
      <AppNavigation />
    </Provider>
  );
}
