import axios from 'axios';

export const rootUrl = 'http://8112bddd7091.ngrok.io';

export default axios.create({
    baseURL: rootUrl,
    // baseURL: 'http://localhost:5000',
    headers: {
        Accept: 'application/json'
    }
})
