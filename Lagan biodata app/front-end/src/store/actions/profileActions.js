export const LOGOUT_UPDATE_PROFILE = 'LOGOUT_UPDATE_PROFILE';

export const FETCH_PROFILE = 'FETCH_PROFILE';
export const fetchProfile = profile => {
    return {
        type: FETCH_PROFILE,
        payload: profile
    };
};

export const SHOW_FIRST_TIME_USER = 'SHOW_FIRST_TIME_USER';
export const showFirstTimeUser = profile => ({
        type: SHOW_FIRST_TIME_USER,
        profile: profile
});

export const UPDATE_PROFILE = 'UPDATE_PROFILE';
export const updateProfile = (userToken, profile) => ({
    type: UPDATE_PROFILE,
    payload: profile
});

export const clearProfile = () => {
    return {
        type: LOGOUT_UPDATE_PROFILE,
    };
}
