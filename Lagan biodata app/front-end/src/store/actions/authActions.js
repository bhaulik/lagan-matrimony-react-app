export const UPDATE_TOKEN = 'UPDATE_TOKEN';
export const updateToken = (value) => {
    return { type: UPDATE_TOKEN, tokenValue: value }
};


export const UPDATE_TOKEN_LOGOUT = 'UPDATE_TOKEN_LOGOUT';
export const signOut = () => {
    return { type: UPDATE_TOKEN_LOGOUT, tokenValue: '' };
}

export const SET_DID_TRY_AL = 'SET_DID_TRY_AL';
export const setDidTryAl = () => {
    return { type: SET_DID_TRY_AL };
};

export const SIGN_UP_ERROR = 'SIGN_UP_ERROR';
export const setSignUpError = (errorMessage) => {
    return { type: SIGN_UP_ERROR, signUpError: errorMessage };
}

export const SIGN_IN_ERROR = 'SIGN_IN_ERROR';
export const setSignInError = (errorMessage) => {
    return { type: SIGN_IN_ERROR, signInError: errorMessage };
}
