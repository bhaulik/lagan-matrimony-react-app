import {rootUrl} from "../../api/matches";
import {fetchProfile, showFirstTimeUser, updateProfile} from "../actions/profileActions";

export const fetchProfileRequest = (userToken) => async(dispatch) => {
        try {
            const response = await fetch(
                rootUrl + '/api/profile/me', {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'x-auth-token': userToken
                    }
                }
            );

            const loadedProfile = await response.json();
            console.log(loadedProfile)

            if (loadedProfile && loadedProfile.firstTimeUser) {
                console.log("INSIDE THE FIRST TIME user")
                loadedProfile.profile && loadedProfile.profile.profile &&
                dispatch(showFirstTimeUser(loadedProfile));
            }
            dispatch(fetchProfile(loadedProfile));

        } catch (err) {
            console.log("could not fetch User profile "+ err );
            //dispatch
        }

};

export const updateProfileRequest = (userToken, profile) => async(dispatch) => {
        try {
            const response = await fetch(
                rootUrl + '/api/profile', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'x-auth-token': userToken
                    },
                    body: JSON.stringify(profile)
                });
            const loadedProfile = await response.json();

            console.log("IN THE THUNK")
            console.log(profile)
            dispatch(updateProfile(userToken, profile));
        } catch (e) {
            console.log(e)
            console.log("could not update profile");
            //dispatch
        }
}
