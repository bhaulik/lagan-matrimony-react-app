import { UPDATE_TOKEN, UPDATE_TOKEN_LOGOUT, SET_DID_TRY_AL, SIGN_UP_ERROR, SIGN_IN_ERROR } from '../actions/authActions';

export const initialState = {
    authToken: null,
    didTryAutoLogin: false,
    signInOrSignUpError: null,
}

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_TOKEN:
            return { ...state, authToken: action.tokenValue, didTryAutoLogin: true }
        case UPDATE_TOKEN_LOGOUT:
            return { ...initialState }
        case SET_DID_TRY_AL:
            return { ...state, didTryAutoLogin: true }
        case SIGN_UP_ERROR:
            return { ...state, signUpError: action.signUpError }
        case SIGN_IN_ERROR:
            return { ...state, signInError: action.signInError }
        default:
            return state;
    }
};

export default authReducer;
