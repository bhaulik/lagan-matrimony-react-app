import Profile from '../../models/Profile';
import { UPDATE_PROFILE, FETCH_PROFILE } from '../actions/profileActions';
import { LOGOUT_UPDATE_PROFILE, SHOW_FIRST_TIME_USER } from '../actions/profileActions';

const initialState = {
    profile: {}//;new Profile(),
}

const profileReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PROFILE:
            return {
                profile: action.payload
            }
        case LOGOUT_UPDATE_PROFILE:
            return {
                profile: initialState
            }
        case SHOW_FIRST_TIME_USER:
            return {
                profile: action.payload
            }

        case UPDATE_PROFILE:
            // create new Profile Object
            console.log("inside reducer")
            return {
                profile: action.payload
            }
            // console.log(action.payload)
            // const uProfile = new Profile(
            //     action.payload.user,
            //     action.payload.profile.name,
            //     action.payload.profile.gender,
            //     action.payload.profile.images,
            //     action.payload.profile.previouslyMarried,
            //     action.payload.profile.friends,
            //     action.payload.profile.requestsSent,
            //     action.payload.profile.requestsReceived,
            //     action.payload.profile.blocked,
            //     action.payload.profile.dateOfBirth,
            //     action.payload.profile.placeOfOrigin,
            //     action.payload.profile.momVillage,
            //     action.payload.profile.momsName,
            //     action.payload.profile.dadsName,
            //     action.payload.profile.currentCountryOfResidence,
            //     action.payload.profile.currentPlaceOfResidence,
            //     action.payload.profile.education,
            //     action.payload.profile.currentProfession,
            //     action.payload.profile.diet,
            //     action.payload.profile.height,
            //     action.payload.profile.weight,
            //     action.payload.profile.horoscopeDetails,
            //     action.payload.profile.likesAndInterest,
            //     action.payload.profile.otherInformation);
            // uProfile.citizenships = action.payload.profile.citizenships;
            return {
                profile: action.payload.profile
            };
        default:
            return state;
    }
}

export default profileReducer;
