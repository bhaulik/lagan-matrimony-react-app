
class Profile {
    constructor(
        user,
        name,
        gender,
        images,
        previouslyMarried,
        friends,
        requestsSent,
        requestsReceived,
        blocked,
        dateOfBirth,
        placeOfOrigin,
        momVillage,
        momsName,
        dadsName,
        currentCountryOfResidence,
        currentPlaceOfResidence,
        education,
        currentProfession,
        diet,
        height,
        weight,
        horoscopeDetails,
        likesAndInterest,
        otherInformation) {
        this.user = user;
        this.name = name;
        this.gender = gender;
        this.images = images;
        this.previouslyMarried = previouslyMarried;
        this.friends = friends;
        this.requestsSent = requestsSent;
        this.requestsReceived = requestsReceived;
        this.blocked = blocked;
        this.dateOfBirth = dateOfBirth;
        this.placeOfOrigin = placeOfOrigin;
        this.momVillage = momVillage;
        this.momsName = momsName;
        this.dadsName = dadsName;
        this.currentCountryOfResidence = currentCountryOfResidence;
        this.currentPlaceOfResidence = currentPlaceOfResidence;
        this.education = education;
        this.currentProfession = currentProfession;
        this.diet = diet;
        this.height = height;
        this.weight = weight;
        this.horoscopeDetails = horoscopeDetails;
        this.likesAndInterest = likesAndInterest;
        this.otherInformation = otherInformation;
    }
}

export default Profile;
