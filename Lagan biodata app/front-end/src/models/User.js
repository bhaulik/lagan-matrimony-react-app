class User {
    constructor(
        email,
        password,
        firstTimeUser,
        deactiveAccount,
        phoneNumber,
        date
    ) {
        this.email = email;
        this.password = password;
        this.firstTimeUser = firstTimeUser;
        this.deactiveAccount = deactiveAccount;
        this.phoneNumber = phoneNumber;
        this.date = date;
    }
}

export default User;