import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import MatchDetailsScreen from '../screens/MatchDetailsScreen';
import MatchesListScreen from '../screens/MatchesListScreen';

const Stack = createStackNavigator();

const MatchesStackScreen = ({ navigation }) => {
    return (
    <Stack.Navigator initialRouteName="MatchesList">
        <Stack.Screen name="MatchesList" component={MatchesListScreen} />
        <Stack.Screen name="MatchDetails" component={MatchDetailsScreen} />
      </Stack.Navigator>
    );

}

const styles = StyleSheet.create({});

export default MatchesStackScreen;
