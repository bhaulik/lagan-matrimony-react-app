import React, {useState} from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import {Text, Button, Input } from 'react-native-elements';
import Spacer from './Spacer';

const AuthForm = ({headerText, errorMessage, onSubmit, submitButtonText}) => {
    
    const [email, setEmail ] = useState('');
    const [password, setPassword ] = useState('');

    return (
        <>
        <Spacer>
            <Text h3>{headerText}</Text>
        </Spacer>
        
        <Spacer>
            <Input 
                label="Email" 
                autoCapitalize="none"
                autoCorrect={false}
                value={email}
                onChangeText={setEmail}
            />
            <Input 
                label="Password"
                autoCapitalize="none"
                autoCorrect={false}
                label="Password"
                value={password}
                onChangeText={setPassword}
                secureTextEntry
            />
        { (errorMessage != null  || errorMessage != '') ? <Text style={styles.errorMessage}>{errorMessage}</Text> : null }
        </Spacer>
        <Spacer>
            <Button title={submitButtonText} onPress={() => onSubmit({email, password})}/>  
        </Spacer>

    </>
    );
}

const styles = StyleSheet.create({
    errorMessage: {
        fontSize: 16,
        color: 'red'
    }
});

export default AuthForm;