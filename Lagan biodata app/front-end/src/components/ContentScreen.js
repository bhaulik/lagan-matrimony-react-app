import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import MatchDetailsScreen from '../screens/MatchDetailsScreen';
import MatchesListScreen from '../screens/MatchesListScreen';
import MatchesStackScreen from './MatchesStackScreen';
import ProfileSections from '../screens/ProfileSections';
import Account from '../screens/AccountScreen';
import PartnerPreferences from '../screens/PartnerPreferencesScreen';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';

const BottomTab = createBottomTabNavigator();

const ContentScreen = ({ navigation }) => {

  return (
    <BottomTab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          if (route.name === 'PartnerPreferences') {
            iconName = focused
              ? 'ios-list-box' : 'ios-list';
          } else if (route.name === 'Matches') {
            iconName = focused ? 'ios-heart-half' : 'ios-heart-half';
          }
          else if (route.name === 'ProfileSections') {
            iconName = focused ? 'md-ice-cream' : 'md-ice-cream';
          }

          // You can return any component that you like here!
          return <Ionicons name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      }}
    >
      <BottomTab.Screen name="PartnerPreferences" options={{ title: 'Match Preference' }} component={PartnerPreferences} />
      <BottomTab.Screen name="Matches" component={MatchesStackScreen} />
      <BottomTab.Screen name="ProfileSections" component={ProfileSections} />
    </BottomTab.Navigator>
  );

}

const styles = StyleSheet.create({});

export default ContentScreen;
