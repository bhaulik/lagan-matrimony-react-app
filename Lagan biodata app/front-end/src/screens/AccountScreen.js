import React, { useState, useEffect, useCallback } from 'react';
import { View, StyleSheet, ActivityIndicator, TouchableOpacity, ScrollView, FlatList, AsyncStorage } from 'react-native';
import { Text, Card, Input, Button, Image, Icon, ListItem, Divider } from 'react-native-elements';
import { useSelector, useDispatch } from 'react-redux';
import * as profileActions from '../store/actions/profileActions'
import * as authActions from '../store/actions/authActions'
import Carousel from '../components/Carousel';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';
import { Roboto_400Regular, Roboto_700Bold, Roboto_900Black } from '@expo-google-fonts/roboto';
import { dummyData } from '../data/Data';
import { fetchProfileRequest } from "../store/thunk/thunk";
import { connect } from 'react-redux';
import {Linking} from "expo/build/deprecated.web";

const AccountScreen = (props) => {
    let { profile, userToken, loadProfileInfo, navigation } = props;

    let [fontsLoaded] = useFonts({
        Inter_900Black,
        Roboto_400Regular,
        Roboto_700Bold,
        Roboto_900Black
    });
    let [isLoadingTimeOutOver, setIsLoadingTimeOutOver] = useState(false);
    const dispatch = useDispatch();

    const logOut = useCallback(async () => {
        console.log('logging out ...');
        AsyncStorage.removeItem('token');
        dispatch(profileActions.clearProfile());
        dispatch(authActions.signOut());
    }, [userToken]);

    useEffect(() => {
        loadProfileInfo(userToken);
    }, []);

    const calculateUserAge = (dob) => {
        const diff_ms = Date.now() - dob.getTime();
        const age_dt = new Date(diff_ms);

        return Math.abs(age_dt.getUTCFullYear() - 1970);
    }

    if ((!fontsLoaded || !profile || Object.keys(profile).length === 0)
        && !isLoadingTimeOutOver) {
        setTimeout(() => {setIsLoadingTimeOutOver(true)}, 10000);
        return (
            <View style={styles.view}>
                <Text style={styles.loading}>Loading your profile, please wait ...</Text>
                <ActivityIndicator size="large" color="blue" />
            </View>
            );
    }
    else if(Object.keys(profile).length === 0 && isLoadingTimeOutOver) {
        return(
            <View style={styles.view}>
                <Text style={styles.nameSection}>
                    Cannot Load User information! Restart app or contact developer
                </Text>
                <Button
                    title="Contact Developer"
                    type="solid"
                    buttonStyle={styles.editProfileButton}
                    onPress={() => Linking.openURL('mailto:support@example.com?subject=SendMail&body=Description') }
                />
            </View>
        );
    }
    else if (profile && profile.firstTimeUser) {
        return (
            <View style={styles.view}>
                <View style={styles.nameSection}>
                    <Text style={styles.nameText}>You are a first time user, please update your profile
                    to be able to see more matches, click the <Text style={{ fontStyle: "italic" }}>Edit Profile Button</Text> below</Text>
                    <Button
                        title="Edit Profile"
                        type="solid"
                        buttonStyle={styles.editProfileButton}
                        onPress={() => {
                            navigation.navigate('EditProfile')
                        }
                        }
                    />
                    <Button
                        title="Log Out"
                        type="solid"
                        buttonStyle={styles.editProfileButton}
                        onPress={logOut}
                    />
                </View>
            </View>
        );
    }
    else {
        return (
            <View style={styles.view}>
                <ScrollView>
                    <Carousel data={dummyData} />
                    <View style={styles.nameSection}>
                        {profile.name && <Text style={styles.nameText}>{profile.name}</Text>}
                        <Text style={styles.otherSection}>Age:
                            <Text style={{ fontWeight: 'bold' }}> {profile.dateOfBirth ?
                                calculateUserAge(new Date(profile.dateOfBirth)) : '_'} years</Text>
                        </Text>
                        {profile.gender &&
                        <Text style={styles.otherSection}>Gender:
                            <Text style={{ fontWeight: 'bold' }}>{profile.gender}</Text>
                        </Text>}
                        {profile.currentCountryOfResidence &&
                        <Text style={styles.otherSection}>Current Country of Residence:
                            <Text style={{ fontWeight: 'bold' }}>{profile.currentCountryOfResidence}</Text>
                        </Text>}
                        {profile.currentPlaceOfResidence &&
                        <Text style={styles.otherSection}>Current Place of Residence:
                            <Text style={{ fontWeight: 'bold' }}>{profile.currentPlaceOfResidence}</Text>
                        </Text>}
                        {profile.placeOfOrigin &&
                        <Text style={styles.otherSection}>Place of Origin:
                            <Text style={{ fontWeight: 'bold' }}>{profile.placeOfOrigin}</Text>
                        </Text>}
                        {profile.momVillage &&
                        <Text style={styles.otherSection}>Mom's Place of Origin:
                            <Text style={{ fontWeight: 'bold' }}>{profile.momVillage}</Text>
                        </Text>}
                        {profile.citizenships &&
                        <Text style={styles.otherSection}>Citizenship(s):
                            <Text style={{ fontWeight: 'bold' }}>{profile.citizenships}</Text>
                        </Text>}
                        {profile.gender &&
                        <Text style={styles.otherSection}>Gender:
                            <Text style={{ fontWeight: 'bold' }}>{profile.gender}</Text>
                        </Text>}
                        {profile.education &&
                        <Text style={styles.otherSection}>Education:
                            <Text style={{ fontWeight: 'normal', fontStyle: "italic" }}>{profile.education}</Text>
                        </Text>}
                        {profile.currentProfession &&
                        <Text style={styles.otherSection}>Profession:
                            <Text style={{ fontWeight: 'bold' }}>{profile.currentProfession}</Text>
                        </Text>}
                        {profile.height &&
                        <Text style={styles.otherSection}>Height:
                            <Text style={{ fontWeight: 'bold' }}>{profile.height}</Text>
                        </Text>}
                        {profile.weight &&
                        <Text style={styles.otherSection}>Weight:
                            <Text style={{ fontWeight: 'bold' }}>{profile.weight}</Text>
                        </Text>}
                        {profile.diet &&
                        <Text style={styles.otherSection}>Diet:
                            <Text style={{ fontWeight: 'bold' }}>{profile.diet}</Text>
                        </Text>}
                        {profile.horoscopeDetails &&
                        <Text style={styles.otherSection}>Horoscope details:
                            <Text style={{ fontWeight: 'bold' }}>{profile.horoscopeDetails}</Text>
                        </Text>}
                        {profile.likesAndInterest &&
                        <Text style={styles.otherSection}>Likes and Interest:
                            <Text style={{ fontWeight: 'normal', fontStyle: "italic" }}>{profile.likesAndInterest}</Text>
                        </Text>}
                        <Divider style={{ backgroundColor: 'blue', marginBottom: 15 }} />
                        {profile.otherInformation && <Text style={styles.otherSection}>Other Information: <Text style={{ fontWeight: 'normal', fontStyle: "italic" }}>{profile.otherInformation}</Text></Text>}
                    </View>
                    <Button
                        title="Edit Profile"
                        type="solid"
                        buttonStyle={styles.editProfileButton}
                        onPress={() => {
                            navigation.navigate('EditProfile')
                        }
                        }
                    />
                    <Button
                        title="Log Out"
                        type="solid"
                        buttonStyle={styles.editProfileButton}
                        onPress={logOut}
                    />
                </ScrollView>
            </View >
        );
    }
}

const mapStateToProps = state => {
    return {
        profile: state.profile.profile,
        userToken: state.auth.authToken
    }
};

const mapDispatchToProps = dispatch => ({
    loadProfileInfo: (userToken) => dispatch(fetchProfileRequest(userToken))
});

const styles = StyleSheet.create({
    btnStyle: {
        marginTop: 50,
    },
    loading: {
      fontSize: 30,
    },
    titleStyle: {
        fontSize: 48,
        marginTop: 80
    },
    profileImageStyle: {
        height: 400,
        width: 400
    },
    view: {
        marginTop: 70,
        marginBottom: 20,
    },
    nameSection: {
        marginLeft: 20,
        marginRight: 20
    },
    nameText: {
        fontSize: 30,
        fontFamily: 'Roboto_900Black',
        marginBottom: 15
    },
    age: {
        fontSize: 25,
        fontFamily: 'Roboto_400Regular',
        marginBottom: 15
    },
    otherSection: {
        fontFamily: 'Roboto_400Regular',
        fontSize: 20,
        marginBottom: 10
    },
    editProfileButton: {
        height: 50,
        margin: 30,
        marginTop: 15,
        borderRadius: 20
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(AccountScreen);
