import React, { useEffect, useCallback } from 'react';
import { View, StyleSheet } from 'react-native';
import { Text, Input, Button } from 'react-native-elements';
import Spacer from '../components/Spacer';
import { useSelector, useDispatch } from 'react-redux';

const PartnerPreferencesScreen = () => {
    const userTokenCurrent = useSelector(state => state.auth.authToken);

    return (
        <View>
            <Text style={styles.titleStyle}>Preferences</Text>
        <Spacer>
        </Spacer>
    </View>
    );
}

const styles = StyleSheet.create({
    btnStyle: {
        marginTop: 50,
    },
    titleStyle: {
        fontSize: 48,
        marginTop: 80

    }
});

export default PartnerPreferencesScreen;
