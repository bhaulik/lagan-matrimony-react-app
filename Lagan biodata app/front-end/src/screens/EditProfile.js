import React, { useState, useEffect, useCallback } from 'react';
import { Image, Button, Header, Divider, Input, Text, colors, CheckBox } from 'react-native-elements';
import { View, StyleSheet, ActivityIndicator, TouchableOpacity, ScrollView, FlatList } from 'react-native';
import {useSelector, useDispatch, connect} from 'react-redux';
import { Inter_900Black } from '@expo-google-fonts/inter';
import DateTimePicker from '@react-native-community/datetimepicker';
import {fetchProfileRequest, updateProfileRequest} from "../store/thunk/thunk";
// import { Picker } from '@react-native-community/picker';
import * as ImagePicker from 'expo-image-picker';

const EditProfile = (props) => {
    let {route, navigation, userToken, profile, loadProfile, updateProfileInfo} = props;
    //const [profile, setProfile] = useState();
    const [images, setImages] = useState(profile &&
        profile.images ? profile.images: []);
    const [profilePic, setProfilePic] = useState(profile &&
        images.length > 0 ? images[0]: []);
    // console.log("inside edit profile component")
    // console.log(profile)
    const [name, setName] = useState(profile && profile.name);
    const [gender, setGender] = useState(profile && profile.gender);
    const [dateOfBirth, setDateOfBirth] = useState(profile && profile.dateOfBirth);
    const [previouslyMarried, setPreviouslyMarried] = useState(profile && profile.previouslyMarried);
    const [placeOfOrigin, setPlaceOfOrigin] = useState(profile && profile.placeOfOrigin);
    const [momVillage, setMomVillage] = useState(profile && profile.momVillage);
    const [currentCountryOfResidence, setCurrentCountryOfResidence ] = useState(profile && profile.currentCountryOfResidence);
    const [citizenships, setCitizenships] = useState(profile && profile.citizenships);
    const [currentPlaceOfResidence, setCurrentPlaceOfResidence] = useState(profile && profile.currentPlaceOfResidence);
    const [education, setEducation] = useState(profile && profile.education);
    const [currentProfession, setProfession] = useState(profile && profile.currentProfession);
    const [diet, setDiet] = useState(profile && profile.diet);
    const [horoscopeDetails, setHoroscope] = useState(profile && profile.horoscope);
    const [likesAndInterest, setLikesAndInterest] = useState(profile && profile.likesAndInterest);
    const [otherInformation, setOtherInformation] = useState(profile && profile.otherInformation);

    const [date, setDate] = useState(new Date(1598051730000));
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);

    const updateProfile = () => {
        //get user profile info from current user profile state
        let profileRequest = {
            name: name,
            gender: gender,
            dateOfBirth: dateOfBirth,
            images: images,
            previouslyMarried: previouslyMarried,
            placeOfOrigin: placeOfOrigin,
            momVillage: momVillage,
            currentCountryOfResidence: currentCountryOfResidence,
            citizenships: citizenships,
            currentPlaceOfResidence: currentPlaceOfResidence,
            education: education,
            currentProfession: currentProfession,
            diet: diet,
            horoscopeDetails: horoscopeDetails,
            likesAndInterest: likesAndInterest,
            otherInformation: otherInformation
        };

        updateProfileInfo(userToken, profileRequest);
        navigation.navigate("AccountScreen");
    }

        useEffect(() => {
            // console.log("Edit Profile, use effect");
            // console.log(profile)
            loadProfile(userToken);
        }, []);

        const onChange = (event, selectedDate) => {
            const currentDate = selectedDate || date;
            setShow(Platform.OS === 'ios');
            setDate(currentDate);
            setDateOfBirth(new Date(currentDate));
        };

        const showMode = (currentMode) => {
            setShow(true);
            setMode(currentMode);
        };

        const showDatepicker = () => {
            showMode('date');
        };

    const addMediaImages = (inputImage) => {
        let imgs = images;

        setImages(imgs);
    };

    const uploadMedia = async () => {
            let permissionResult = await ImagePicker.requestCameraRollPermissionsAsync();

            if (permissionResult.granted === false) {
                alert("Permission to access camera roll is required!");
                return;
            }

            let pickerResult = await ImagePicker.launchImageLibraryAsync();

            setImages([pickerResult.uri]);
    };

    const uploadProfilePic = async () => {
        let permissionResult = await ImagePicker.requestCameraRollPermissionsAsync();

        if (permissionResult.granted === false) {
            alert("Permission to access camera roll is required!");
            return;
        }

        let pickerResult = await ImagePicker.launchImageLibraryAsync();
        console.log(pickerResult);
        if(!pickerResult.cancelled) {
            if(images && images.length > 0){
                console.log("first image is");
                console.log(images[0]);
                setImages(img);
            }
            else {
                setImages([pickerResult]);
            }

        }

    }

    return (
            <>
                <ScrollView>
                    <View style={styles.parent}>
                        <Image
                            source={{uri: images && images.length > 0 ?
                             images[0].uri : "http://clipart-library.com/img/1934564.jpg"}}
                            style={styles.imageStyling}
                        />
                        <Image
                            source={{uri: images && images.length > 0 ?
                                    images[1].uri : "http://clipart-library.com/img/1934564.jpg"}}
                            style={styles.imageStyling}
                        />
                        <Image
                            source={{uri: images && images.length > 0 ?
                                    images[2].uri : "http://clipart-library.com/img/1934564.jpg"}}
                            style={styles.imageStyling}
                        />
                    </View>
                    <View style={styles.parent}>
                        <Image
                            source={{uri: images && images.length > 0 ?
                                    images[3].uri : "http://clipart-library.com/img/1934564.jpg"}}
                            style={styles.imageStyling}
                        />
                        <Image
                            source={{uri: images && images.length > 0 ?
                                    images[4].uri : "http://clipart-library.com/img/1934564.jpg"}}
                            style={styles.imageStyling}
                        />
                        <Image
                            source={{uri: images && images.length > 0 ?
                                    images[5].uri : "http://clipart-library.com/img/1934564.jpg"}}
                            style={styles.imageStyling}
                        />
                    </View>
                    <View style={styles.addMediaView}>
                        <Button title='Update Profile Pic'
                                buttonStyle={{...styles.addMediaBtn, marginBottom: 20} }
                                onPress={uploadProfilePic}
                        />
                        <Button title='Add Media'
                                buttonStyle={styles.addMediaBtn}
                                onPress={uploadMedia}
                        />
                    </View>
                    <View h5 style={{...styles.aboutHeader, ...styles.inputViewStyle}}>
                        <Input
                            label="Name"
                            labelStyle={styles.labelStyle}
                            inputContainerStyle={styles.inputContainerStyle}
                            inputStyle={styles.inputStyle}
                            placeholder='Enter Name'
                            value={name}
                            onChangeText={value => {
                                setName(value);
                            }}
                            // errorStyle={{ color: 'red' }}
                            // errorMessage='ENTER A VALID ERROR HERE'
                        />
                        <View style={styles.genderPickerStyle}>
                            <Text style={{
                                color: 'gray',
                                padding: 10,
                                paddingTop: 0,
                                fontSize: 17,
                                paddingLeft: 0,
                                marginLeft: 0, ...styles.labelStyle
                            }}>Gender</Text>
                            <CheckBox
                                center
                                title='Male'
                                checkedIcon='dot-circle-o'
                                uncheckedIcon='circle-o'
                                checked={gender && gender.toLowerCase() === 'male'}
                                onPress={() => setGender('male')}
                            />
                            <CheckBox
                                center
                                title='Female'
                                checkedIcon='dot-circle-o'
                                uncheckedIcon='circle-o'
                                checked={gender && gender.toLowerCase() === 'female'}
                                onPress={() => setGender('female')}
                            />
                        </View>
                        <View style={styles.genderPickerStyle}>
                            <Text style={{
                                color: 'gray',
                                padding: 10,
                                paddingTop: 0,
                                fontSize: 17,
                                paddingLeft: 0,
                                marginLeft: 0, ...styles.labelStyle
                            }}>Previously Married</Text>
                            <CheckBox
                                center
                                title='Yes'
                                checkedIcon='dot-circle-o'
                                uncheckedIcon='circle-o'
                                checked={previouslyMarried}
                                onPress={() => setPreviouslyMarried(true)}
                            />
                            <CheckBox
                                center
                                title='No'
                                checkedIcon='dot-circle-o'
                                uncheckedIcon='circle-o'
                                checked={!previouslyMarried}
                                onPress={() => setPreviouslyMarried(false)}
                            />
                        </View>
                        <View>
                            <View>
                                <Text style={{
                                    color: 'gray',
                                    padding: 10,
                                    paddingTop: 0,
                                    fontSize: 17, ...styles.labelStyle
                                }}>Date of Birth</Text>
                            </View>
                            <View>
                                <Button buttonStyle={styles.dateTimePickerStyleBtn}
                                        onPress={showDatepicker}
                                        title={dateOfBirth ?
                                            new Date(dateOfBirth).toDateString() :
                                            new Date().toDateString()
                                        }
                                />
                            </View>
                        </View>
                        {show && (
                            <DateTimePicker
                                testID="dateTimePicker"
                                value={dateOfBirth ?
                                    new Date(dateOfBirth) :
                                    new Date()}
                                mode={mode}
                                is24Hour={true}
                                display="default"
                                onChange={onChange}
                            />
                        )}

                        <View style={{...styles.inputViewStyle, marginTop: 10}}>
                            <Input
                                label="Place of Origin in India (Village, City, Town, State etc.)"
                                labelStyle={styles.labelStyle}
                                inputContainerStyle={styles.inputContainerStyle}
                                inputStyle={styles.inputStyle}
                                onChangeText={value => setPlaceOfOrigin(value)}
                                value={placeOfOrigin}
                            />
                        </View>

                        <View style={styles.inputViewStyle}>
                            <Input
                                label="Mom's place of origin (Optional field)"
                                labelStyle={styles.labelStyle}
                                inputContainerStyle={styles.inputContainerStyle}
                                inputStyle={styles.inputStyle}
                                onChangeText={value => setMomVillage(value)}
                                value={momVillage}
                            />
                        </View>

                        <View style={styles.inputViewStyle}>
                            <Input
                                label="Current City of Residence"
                                labelStyle={styles.labelStyle}
                                inputContainerStyle={styles.inputContainerStyle}
                                inputStyle={styles.inputStyle}
                                onChangeText={value => setCurrentPlaceOfResidence(value)}
                                value={currentPlaceOfResidence}
                            />
                        </View>

                        <View style={styles.inputViewStyle}>
                            <Input
                                label={"Current Country of Residence\n"}
                                labelStyle={styles.labelStyle}
                                inputContainerStyle={styles.inputContainerStyle}
                                inputStyle={styles.inputStyle}
                                onChangeText={value => setCurrentCountryOfResidence(value)}
                                value={currentCountryOfResidence}
                            />
                        </View>

                        <View style={styles.inputViewStyle}>
                            <Input
                                label={"Citizenship(s) or Permanent Residence Card or Green Card \n(example: Kenya, Canada, USA)"}
                                labelStyle={styles.labelStyle}
                                inputContainerStyle={styles.inputContainerStyle}
                                inputStyle={styles.inputStyle}
                                onChangeText={value => setCitizenships(value)}
                                value={citizenships}
                            />
                        </View>

                        <View style={styles.inputViewStyle}>
                            <Input
                                label={"Education \n(Indicate if ongoing or completed and which university or college including city or town of institution)"}
                                multiline={true}
                                numberOfLines={10}
                                inputStyle={{borderColor: "gray", borderWidth: 0.5, borderRadius: 10,}}
                                labelStyle={styles.labelStyle}
                                inputContainerStyle={styles.inputContainerStyle}
                                inputStyle={styles.inputStyle}
                                onChangeText={value => setEducation(value)}
                                value={education}
                            />
                        </View>


                        <View style={styles.inputViewStyle}>
                            <Input
                                label="Current Profession (e.g. doctor, pharmacist, lawyer, software engineer etc.)"
                                labelStyle={styles.labelStyle}
                                inputContainerStyle={styles.inputContainerStyle}
                                inputStyle={styles.inputStyle}
                                onChangeText={value => setProfession(value)}
                                value={currentProfession}
                            />
                        </View>

                        <View style={styles.inputViewStyle}>
                            <Input
                                label={"Diet \n(Indicate if vegan, vegetarian, non-vegetarian, jain or any other)"}
                                labelStyle={styles.labelStyle}
                                inputContainerStyle={styles.inputContainerStyle}
                                inputStyle={styles.inputStyle}
                                onChangeText={value => setDiet(value)}
                                value={diet}
                            />
                        </View>

                        <View style={styles.inputViewStyle}>
                            <Input
                                label="Horoscope Details (Optional field)"
                                labelStyle={styles.labelStyle}
                                inputContainerStyle={styles.inputContainerStyle}
                                inputStyle={styles.inputStyle}
                                onChangeText={value => setHoroscope(value)}
                                value={horoscopeDetails}
                            />
                        </View>

                        <View style={styles.inputViewStyle}>
                            <Input
                                label="Likes and Interest (Indicate hobbies e.g drawing, gym, cycling, cooking, badminton etc.)"
                                multiline={true}
                                numberOfLines={15}
                                // inputStyle={{ borderColor: "gray", borderWidth: 0.5, borderRadius: 10, }}
                                labelStyle={styles.labelStyle}
                                inputContainerStyle={styles.inputContainerStyle}
                                inputStyle={styles.inputStyle}
                                onChangeText={value => setLikesAndInterest(value)}
                                value={likesAndInterest}
                            />
                        </View>

                        <View style={styles.inputViewStyle}>
                            <Input
                                label="Other Information that you may want to provide e.g. religious sector etc."
                                multiline={true}
                                numberOfLines={15}
                                labelStyle={styles.labelStyle}
                                inputContainerStyle={styles.inputContainerStyle}
                                inputStyle={styles.inputStyle}
                                onChangeText={value => setOtherInformation(value)}
                                value={otherInformation}
                            />
                        </View>
                    </View>
                    <View style={styles.uploadProfileButton}>
                        <Button
                            title='Update Profile'
                            buttonStyle={styles.uploadProfileButton}
                            onPress={updateProfile}
                        />
                    </View>
                </ScrollView>
            </>
        );
}
const mapStateToProps = state => {
    console.log("Edit Profile");
    console.log(state);
    return {
        profile: state.profile.profile,
        userToken: state.auth.authToken
    }
};

const mapDispatchToProps = dispatch => ({
    loadProfile: (userToken) => dispatch(fetchProfileRequest(userToken)),
    updateProfileInfo: (userToken, profile) => dispatch(updateProfileRequest(userToken, profile))
});

const styles = StyleSheet.create({
    parent: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-evenly",

    },
    imageStyling: {
        marginLeft: 5,
        marginBottom: 5,
        marginTop: 10,
        paddingLeft: 60,
        paddingRight: 60,
        paddingTop: 110,
        paddingBottom: 100,
        borderRadius: 6,
        transform: [{ scale: 1.0 }]
    },
    aboutHeader: {
        margin: 10,
        marginTop: 20,
        paddingTop: 30,
        backgroundColor: "white",
        borderRadius: 5,
    },
    headerText: {
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 15,
    },
    addMediaView: {
        margin: 10,
        padding: 10,
        marginBottom: 10
    },
    addMediaBtn: {
        borderRadius: 10,
        height: 50,
    },
    uploadProfileButton: {
        margin: 10,
        padding: 15,
        marginBottom: 20,
        borderRadius: 10,
    },
    dateTimeParentStyle: {
        display: "flex",
        flexDirection: "row",
        justifyContent: 'space-between',

    },
    dobStyleInput: {
        margin: 5,
        marginRight: 5,
        marginLeft: 5,
        paddingRight: 5,
        paddingLeft: 0,
        backgroundColor: "black",
    },
    dateTimePickerStyleBtn: {
        height: 50,
        marginRight: 10,
        marginLeft: 10,
        paddingRight: 5,
        paddingLeft: 5,
        borderStyle: "solid",
        backgroundColor: 'gray',
        borderBottomColor: 'black',
    },
    genderPickerStyle: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        marginLeft: 10,
        marginBottom: 10
    },
    labelStyle: {
        color: 'black',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 17,
    },
    inputContainerStyle: {
        borderBottomWidth: 0,
    },
    inputStyle: {
        backgroundColor: 'white',
        borderRadius: 10,
        marginTop: 15,
    },
    inputViewStyle: {
        backgroundColor: '#EAEDEE',
        padding: 5
    },

});

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
