import React, { useState, useEffect, useCallback } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import matchesApi from '../api/matches';
import { AsyncStorage } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { updateToken, setSignUpError } from '../store/actions/authActions';
import AuthForm from '../components/AuthForm';
import NavLink from '../components/NavLink';

const SignupScreen = ({ navigation }) => {
    const userTokenCurrent = useSelector(state => state.auth.authToken);
    const signUpError = useSelector(state => state.auth.signUpError); //can remove
    const [signUpErrorLocal, setsignUpErrorLocal] = useState('');
    const dispatch = useDispatch();
    const updateUserTokenHandler = useCallback((token) => {
        dispatch(updateToken(token));
    }, [dispatch, userTokenCurrent]);

    const setSignUpErrorMessage = useCallback((error) => {
        dispatch(setSignUpError(error));
    }, [dispatch, signUpError]);

    useEffect(() => {

        //updateUserTokenHandler({userTokenCurrent});
    }, [updateUserTokenHandler]);

    const signup = async ({ email, password }) => {
        try {
            await AsyncStorage.removeItem('token');
            const response = await matchesApi.post('/api/users', { email, password });
            await AsyncStorage.setItem('token', response.data.token);
            updateUserTokenHandler(response.data.token);
            await AsyncStorage.setItem('token', response.data.token);
        } catch (err) {
            console.log('error signing up!');
            //might not need global dispatch
            //setSignUpErrorMessage('You might have used this email to sign up already');
            setsignUpErrorLocal('You might have used this email to sign up already');
        }
    };

    return (

        <View style={styles.container}>
            <AuthForm
                headerText="Sign up for Desi Dating"
                errorMessage={signUpErrorLocal}
                onSubmit={signup}
                submitButtonText="Sign Up"
            />
            <NavLink
                navigation={navigation}
                text="Already have an account? Sign in instead"
                routeName='SigninScreen'
            />
        </View>
    );
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        marginBottom: 250,
    },
    errorMessage: {
        fontSize: 16,
        color: 'red'
    },

});

export default SignupScreen;
