import React, { useEffect, useCallback } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SigninScreen from './SigninScreen';
import SignupScreen from './SignupScreen';
import ContentScreen from '../components/ContentScreen';
import { useSelector, useDispatch } from 'react-redux';
import { updateToken, setSignInError, setSignUpError, didTryAutoLogin } from '../store/actions/authActions';
import { AsyncStorage } from 'react-native';

const Stack = createStackNavigator();

export function AppNavigation({ navigation }) {
  const authToken = useSelector(state => state.auth.authToken);
  const didTryAutoLogin = useSelector(state => state.auth.didTryAutoLogin);
  const dispatch = useDispatch();

  useEffect(() => {
    //refactor to not call this in case a user signs in
    const tryLogin = async () => {
      console.log('auto logging...');
      try {
        const userData = await AsyncStorage.getItem('token');
        if (!userData) {
          dispatch(setDidTryAl());
          navigation.navigate('SigninScreen');
          return;
        }
        const token = userData;
        // const dispatch = useDispatch();
        // //can check expiration if done
        dispatch(updateToken(token));
      } catch (e) {
        console.log('could not get user data')
      }

    };

    tryLogin();
  }, [didTryAutoLogin])


  return (
    <>
      <NavigationContainer>
        {
          // check expiration
          (authToken == null || authToken === '') ?
            (
              <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen name="SigninScreen" component={SigninScreen} />
                <Stack.Screen name="SignupScreen" component={SignupScreen} />
              </Stack.Navigator>
            ) :
            (
              <ContentScreen />
            )
        }
      </NavigationContainer>
    </>
  );
}



