import React, { useState, useEffect, useCallback } from 'react';
import { View, StyleSheet, Text, AsyncStorage } from 'react-native';
import AuthForm from '../components/AuthForm';
import NavLink from '../components/NavLink';
import { updateToken, setSignInError, setSignUpError } from '../store/actions/authActions';
import matchesApi from '../api/matches';
import { useSelector, useDispatch } from 'react-redux';

const SigninScreen = ({ navigation }) => {
    const [signInErrorLocal, setsignInErrorLocal] = useState('');
    const userTokenCurrent = useSelector(state => state.auth.authToken);

    const dispatch = useDispatch();
    const updateUserTokenHandler = useCallback((token) => {
        dispatch(updateToken(token));
    }, [dispatch]);

    const signin = async ({ email, password }) => {
        try {
            const response = await matchesApi.post('/api/user', { email, password });
            updateUserTokenHandler(response.data.token);
            try {
                await AsyncStorage.setItem('token', response.data.token);
            } catch (e) {
                console.log('could not update token locally');
            }

        } catch (err) {
            console.log('error signing in!');
            //dispatch sign in error and render it
            console.log(err);
            //can prevent a global error dispatch
            //dispatch(setSignInError('Invalid username or password'));
            setsignInErrorLocal('Invalid username or password');
        }
    };

    return (
        <View style={styles.container}>
            <AuthForm
                headerText="Sign in for Desi Dating"
                errorMessage={signInErrorLocal}
                onSubmit={signin}
                submitButtonText="Sign In"
            />
            <NavLink
                navigation={navigation}
                text="Don't have an account? Sign up instead"
                routeName='SignupScreen'
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        marginBottom: 250,
    },
    errorMessage: {
        fontSize: 16,
        color: 'red'
    },
});

export default SigninScreen;
