import React, { useState } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, ActivityIndicator } from 'react-native'
import { Card, ListItem, Button, Icon } from 'react-native-elements'
import { FlatList } from 'react-native-gesture-handler';

let users_data = [
  {
    name: 'brynn',
    avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg'
  },
  {
    name: 'Jianah',
    avatar: 'https://i.pravatar.cc/150?img=21'
  },
  {
    name: 'brynn',
    avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg'
  },
  {
    name: 'Jianah',
    avatar: 'https://i.pravatar.cc/150?img=21'
  },
  {
    name: 'brynn',
    avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg'
  },
  {
    name: 'Jianah',
    avatar: 'https://i.pravatar.cc/150?img=21'
  },

];

let _renderItem = ({ item, index }) => {
  let { container, cardText, card, cardImage } = styles
  //console.log(item);
  return (
    <>
      <View style={container}>
        <View style={card}>
          <Image style={cardImage} source={{ uri: item.avatar }} />
          <Text style={cardText}>{item.name}</Text>
          <Button title='View Profile'></Button>
        </View>
      </View>
    </>
  );
}

const MatchesListScreen = ({ navigation }) => {

  let { container, cardText, card, cardImage } = styles
  const [users, AddUsers] = useState(users_data);

  if (users.length === 0) {
    return (
      <View style={styles.loader}>
        <ActivityIndicator size="large" />
      </View>
    );
  }


  return (
    <>
      <FlatList
        style={container}
        data={users}
        keyExtractor={(item, index) => index.toString()}
        renderItem={_renderItem}
      />
    </>
  )

}

const styles = StyleSheet.create({
  container: {
    marginTop: 15,
    marginBottom: 5,
    borderStyle: "solid",
    borderColor: 'black'
  },
  cardText: {
    fontSize: 16,
    padding: 8
  },
  card: {
    backgroundColor: '#fff',
    marginBottom: 10,
    marginLeft: '2%',
    width: '96%',
    shadowColor: '#000',
    shadowOpacity: 1,
    shadowOffset: {
      width: 5,
      height: 5
    },
    padding: 10
  },
  cardImage: {
    width: '100%',
    height: 200,
    resizeMode: 'cover',
    borderRadius: 10
  },
  loader: {
    flex: 1,
    marginTop: 20,
    alignItems: "center"
  }
});
export default MatchesListScreen;