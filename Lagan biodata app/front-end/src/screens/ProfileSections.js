import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AccountScreen from './AccountScreen';
import EditProfile from './EditProfile';
import { useSelector, useDispatch } from 'react-redux';

const Stack = createStackNavigator();

const ProfileSections = () => {
    return (
        <>
            <Stack.Navigator initialRouteName="AccountScreen">
                <Stack.Screen name="AccountScreen" component={AccountScreen}/>
                <Stack.Screen name="EditProfile" component={EditProfile}/>
            </Stack.Navigator>
        </>
    );
}

export default ProfileSections;
