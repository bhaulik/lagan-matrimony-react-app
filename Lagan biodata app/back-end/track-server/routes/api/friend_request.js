const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const { check, validationResult } = require('express-validator');

const Profile = require('../../models/Profile');
const User = require('../../models/User');
const ACCEPT_REQUEST = 'accept';
const REJECT_REQUEST = 'reject';

//@route POST api/friend_request
//@desc send a friend request to another request
//@access Private
router.post('/', auth, async (req, res) => {

    const user = await User.findById(req.user.id).select('-password');
    if (!user) {
        return res.status(400).json({ msg: 'User not found' });
    }

    const {
        user_id
    } = req.body;

    if (req.user.id === user_id) {
        return res.status(400).json({ msg: 'Cannot send a friend request to the same user' });
    }

    const userToSendRequestTo = await User.findById(user_id).select('-password');
    if (!userToSendRequestTo) {
        return res.status(400).json({ msg: 'Cannot send a friend request' });
    }

    try {
        let requestProfile = await Profile.findOne({ user: user_id }); //user profile of the user to which we send a request
        let currentUserProfile = await Profile.findOne({ user: req.user.id });
        let { requestsReceived } = requestProfile;
        let requestBlockedUsers = requestProfile.blocked;
        let requestFriends = requestProfile.friends;
        let { requestsSent, blocked, friends } = currentUserProfile;
        // console.log(requests);
        // console.log(user._id);
        if (requestProfile && currentUserProfile &&
            requestsReceived.indexOf(req.user.id) === -1 &&
            requestBlockedUsers.indexOf(req.user.id) === -1 &&
            requestFriends.indexOf(req.user.id) === -1 &&
            requestsSent.indexOf(user_id) === -1 &&
            blocked.indexOf(user_id) === -1 &&
            friends.indexOf(user_id) === -1) {
            requestsReceived.push(req.user.id);// or requests.push(user._id);
            requestsSent.push(user_id);
            // add to the list of the friends
            requestProfile = await Profile.findOneAndUpdate({ user: user_id }, { $set: { requestsReceived: requestsReceived } }, { new: true });
            currentUserProfile = await Profile.findOneAndUpdate({ user: req.user.id }, { $set: { requestsSent: requestsSent } }, { new: true });
        } else {
            return res.status(400).json({ msg: 'Cannot send a friend request.' });
        }

        // save the new collection to db
        //await userToSendRequestTo.save();

        requestProfile = new Profile(requestProfile);
        currentUserProfile = new Profile(currentUserProfile);
        await requestProfile.save();
        await currentUserProfile.save();
        return res.status(400).json({ msg: 'Friend request sent successfully' });

    } catch (err) {
        console.log(err.message);
        return res.status(500).send('Server Error');
    }

});

//@route POST api/friend_request/respond
//@desc accept a friend request to another request
// remove from personal requests received, add to friends & remove from other users friend request sent, add to friends array
// body @param1 user_id - the user id request to accept @param2 - type: accept/reject
//@access Private
router.post('/respond', auth, async (req, res) => {
    const user = await User.findById(req.user.id).select('-password');
    if (!user) {
        return res.status(400).json({ msg: 'User not found' });
    }

    const {
        user_id,
        type_request
    } = req.body;

    if (!(type_request === ACCEPT_REQUEST || type_request === REJECT_REQUEST)) {
        return res.status(400).json({ msg: 'Invalid values in body request' });
    }

    if (req.user.id === user_id) {
        return res.status(400).json({ msg: 'Unexpected Database state, contact developer for troubleshooting' });
    }

    const userToAcceptAsFriend = await User.findById(user_id).select('-password');
    if (!userToAcceptAsFriend) {
        return res.status(400).json({ msg: 'Cannot accept the friend request' });
    }

    try {
        let friendProfile = await Profile.findOne({ user: user_id }); //user profile of the user to which we send a request
        let currentUserProfile = await Profile.findOne({ user: req.user.id });
        let { requestsSent } = friendProfile;
        let friendProfile_blocked = friendProfile.blocked;
        let friendProfile_friends = friendProfile.friends;
        let { requestsReceived, friends, blocked } = currentUserProfile;
        // console.log(requests);
        // console.log(user._id);
        if (friendProfile && currentUserProfile &&
            requestsSent.indexOf(req.user.id) !== -1 &&
            requestsReceived.indexOf(user_id) !== -1 &&
            blocked.indexOf(user_id) === -1 &&
            friends.indexOf(user_id) === -1 &&
            type_request === ACCEPT_REQUEST) {
            requestsReceived.pop(user_id);
            requestsSent.pop(req.user.id);

            friends.push(user_id);
            friendProfile_friends.push(req.user.id);

            // add to the list of the friends
            friendProfile = await Profile.findOneAndUpdate({ user: user_id }, { $set: { requestsSent: requestsSent, friends: friendProfile_friends } }, { new: true });
            currentUserProfile = await Profile.findOneAndUpdate({ user: req.user.id }, { $set: { requestsReceived: requestsReceived, friends: friends } }, { new: true });
        }
        else if (friendProfile && currentUserProfile &&
            requestsSent.indexOf(req.user.id) !== -1 &&
            requestsReceived.indexOf(user_id) !== -1 &&
            friends.indexOf(user_id) === -1 &&
            blocked.indexOf(user_id) === -1 &&
            type_request === REJECT_REQUEST) {
            requestsReceived.pop(user_id);
            requestsSent.pop(req.user.id);

            blocked.push(user_id);
            friendProfile_blocked.push(req.user.id);
            // add to the blocked users
            friendProfile = await Profile.findOneAndUpdate({ user: user_id }, { $set: { requestsSent: requestsSent, blocked: friendProfile_blocked } }, { new: true });
            currentUserProfile = await Profile.findOneAndUpdate({ user: req.user.id }, { $set: { requestsReceived: requestsReceived, blocked: blocked } }, { new: true });
        }
        else {
            return res.status(400).json({ msg: 'Cannot respond the friend request..' });
        }

        // save the new collection to db
        //await userToSendRequestTo.save();

        friendProfile = new Profile(friendProfile);
        currentUserProfile = new Profile(currentUserProfile);
        await friendProfile.save();
        await currentUserProfile.save();
        return res.status(400).json({ msg: 'Friend request accepted successfully' });

    } catch (err) {
        console.log(err.message);
        return res.status(500).send('Server Error');
    }

});

module.exports = router;