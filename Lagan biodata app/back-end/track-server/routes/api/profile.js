const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const { check, validationResult } = require('express-validator');

const Profile = require('../../models/Profile');
const User = require('../../models/User');

//@route GET api/profile/me
//@desc Get current user's profile
//@access Private
router.get('/me', auth, async (req, res) => {

    try {
        const profile = await Profile.findOne({ user: req.user.id }).populate(//'user',
            ['phoneNumber', 'firstTimeUser', 'deactiveAccount']);
        const user = await User.findById(req.user.id).select('-password');

        if (!user) {
            res.status(400).json({ msg: 'User not found' });
        }

        //console.log(user);
        // if the user is a first time user, return a message to indicate this
        if (user && user.firstTimeUser) {
            return res.json(user);
        }

        if (!profile) {
            return res.status(400).json({ msg: 'There is no profile for this user' });
        }
        let result = {...profile.toObject(), ...user.toObject()};
        result["profile_id"] = profile.toObject()["_id"]; //since user id will override it
        res.json(result);

    } catch (err) {
        console.log(err.message);
        res.status(500).send('Server Error');
    }
});

//@route POST api/profile
//@desc Create or update user profile
//@access Private
router.post('/', [
    auth,
    [
        check('name', 'Name is required').not().isEmpty(),
        check('gender', 'Gender is required').not().isEmpty(),
        check('dateOfBirth', 'Date of Birth is required').not().isEmpty(),
        check('placeOfOrigin', 'placeOfOrigin is required').not().isEmpty(),
        check('currentCountryOfResidence', 'currentCountryOfResidence is required').not().isEmpty(),
    ]
], auth, async (req, res) => {
    //console.log(req)
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
    }
    const user = await User.findById(req.user.id).select('-password');
    if (!user) {
        return res.status(400).json({ msg: 'User not found' });
    }

    const {
        name,
        gender,
        dateOfBirth,
        placeOfOrigin,
        currentCountryOfResidence,
        currentPlaceOfResidence,
        citizenships,
        education,
        currentProfession,
        diet,
        height,
        weight,
        horoscopeDetails,
        images,
        previouslyMarried,
        momVillage,
        momsName,
        dadsName,
        likesAndInterest,
        otherInformation,
    } = req.body;

    // Build profile object
    const profileFields = {};
    profileFields.user = req.user.id;
    if (name) profileFields.name = name;
    if (gender) profileFields.gender = gender;
    // if (dateOfBirth) {
    //     const ddmmyyyyArray = dateOfBirth.split('-');
    //     const day = ddmmyyyyArray[0];
    //     //https://javascript.info/date - month count starts with 0 - LOL
    //     const month = ddmmyyyyArray[1] - 1;
    //     const year = ddmmyyyyArray[2];
    //     // console.log('MONTH IS ' + month);
    //     const newlyFormattedDateTime = new Date(year, month, day);
    //     profileFields.dateOfBirth = newlyFormattedDateTime;
    // }
    if (dateOfBirth) profileFields.dateOfBirth = dateOfBirth
    if (placeOfOrigin) profileFields.placeOfOrigin = placeOfOrigin;
    if (currentCountryOfResidence) profileFields.currentCountryOfResidence = currentCountryOfResidence;
    if (likesAndInterest) profileFields.likesAndInterest = likesAndInterest;
    if (otherInformation) profileFields.otherInformation = otherInformation;
    if (currentPlaceOfResidence) profileFields.currentPlaceOfResidence = currentPlaceOfResidence;
    if (citizenships) profileFields.citizenships = citizenships;
    if (education) profileFields.education = education;
    if (currentProfession) profileFields.currentProfession = currentProfession;
    if (diet) profileFields.diet = diet;
    if (height) profileFields.height = height;
    if (weight) profileFields.weight = weight;
    if (horoscopeDetails) profileFields.horoscopeDetails = horoscopeDetails;
    if (images) profileFields.images = images;
    if (previouslyMarried) profileFields.previouslyMarried = previouslyMarried;
    if (momVillage) profileFields.momVillage = momVillage;
    if (momsName) profileFields.momsName = momsName;
    if (dadsName) profileFields.dadsName = dadsName;

    let updatedUser = null;
    try {
        let profile = await Profile.findOne({ user: req.user.id });
        // let user = await User.findById(req.user.id);
        if (profile && user) {
            //Update the profile and the flag in the user document because the user is not a first time user anymore
            updatedUser = await User.findOneAndUpdate({ _id: req.user.id }, { $set: { firstTimeUser: false } }, { returnNewDocument: true }).select('-password');
            console.log(updatedUser);
            //updatedUser = new User(updatedUser);
            await updatedUser.save();
            profile = await Profile.findOneAndUpdate({ user: req.user.id }, { $set: profileFields }, { new: true });
            console.log(profile);
            return res.json(profile);
        }

        // Create
        updatedUser = await User.findOneAndUpdate({ _id: req.user.id }, { $set: { firstTimeUser: false } }, { returnNewDocument: true }).select('-password');
        await updatedUser.save();
        profile = new Profile(profileFields);
        await profile.save();
        res.json(profile);

    } catch (err) {
        console.log(err.message);
        return res.status(500).send('Server Error');
    }

});

//@route GET api/profile/all
//@desc Get all profiles but only possible if you are a valid user in the system so pass auth
//@access Public
router.get('/all', auth, async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
    }
    try {
        //get all Profiles
        let profile = await Profile.find().populate('user', ['email', 'firstTimeUser', 'deactiveAccount',
            'phoneNumber', 'date']);
        return res.json(profile);

    } catch (err) {
        console.log(err.message);
        return res.status(500).send('Server Error');
    }
});


//@route GET api/profile/user/:user_id
//@desc Get profile by user Id
//@access private to only the users of the application
router.get('/user/:user_id', auth, async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
    }
    try {
        // get profile by user id
        let profile = await Profile.findOne({ user: req.params.user_id }).populate('user', ['email', 'firstTimeUser', 'deactiveAccount', 'phoneNumber', 'date']);

        if (!profile) return res.status(400).json({ msg: 'Profile not found' })
        return res.json(profile);

    } catch (err) {
        console.log(err.message);
        if (err.kind == 'ObjectId') {
            return res.status(400).json({ msg: 'Profile not found' });
        }
        return res.status(500).send('Server Error');
    }
});

//@route DELETE api/profile
//@desc Delete Profile, User and Posts
//@access private to only the user with the token
router.delete('/', auth, async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
    }
    try {

        const user = await User.findById(req.user.id).select('-password');
        if (!user) {
            res.status(400).json({ msg: 'User not found' });
        }
        //remove  profile
        await Profile.findOneAndRemove({ user: req.user.id });

        // remove user
        await User.findOneAndRemove({ _id: req.user.id });

        return res.json({ msg: 'User removed' });

    } catch (err) {
        console.log(err.message);
        if (err.kind == 'ObjectId') {
            return res.status(400).json({ msg: 'Profile not found' });
        }

        return res.status(500).send('Server Error');
    }
});

//@route GET api/profile/user/:user_id
//@desc Get profile by user Id
//@access private to only the users of the application
router.get('/user/:user_id', auth, async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
    }
    try {
        // get profile by user id
        let profile = await Profile.findOne({ user: req.params.user_id }).populate('user', ['email', 'firstTimeUser', 'deactiveAccount', 'phoneNumber', 'date']);

        if (!profile) return res.status(400).json({ msg: 'Profile not found' })
        return res.json(profile);

    } catch (err) {
        console.log(err.message);
        if (err.kind == 'ObjectId') {
            return res.status(400).json({ msg: 'Profile not found' });
        }
        return res.status(500).send('Server Error');
    }
});

//@route DELETE api/profile/all
//@desc Delete all Profile, User and Posts
//@access private to only the user with the token
router.delete('/all', auth, async (req, res) => {
    try {
        const users = await Profile.find({});
        if (users && users.length === 0) {
            return res.json({ msg: "users don't exist" });
        }

        //delete users
        await User.deleteMany({});
        //remove profiles
        await Profile.deleteMany({});

        return res.json({ msg: 'Users and Profiles removed' });

    } catch (err) {
        console.log(err.message);
        return res.status(500).send('Server Error');
    }
});

module.exports = router;
