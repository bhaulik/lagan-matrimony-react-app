const express = require('express');
const connectDB = require('./config/db');

const app = express();

// connect db
connectDB();

//Init middleware
app.use(express.json({ extended: false }))

app.get('/', (req, res) => res.send('API Running'));

//Define routes
app.use('/api/users', require('./routes/api/users'));
app.use('/api/user', require('./routes/api/user'));
app.use('/api/profile', require('./routes/api/profile'));
app.use('/api/friend_request', require('./routes/api/friend_request'));

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));