const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    firstTimeUser: {
        type: Boolean,
        default: true
    },
    deactiveAccount: {
        type: Boolean,
        default: false,
    },
    phoneNumber: {
        type: Number
    },
    date: {
        type: Date,
        default: Date.now
    }
});

module.exports = User = mongoose.model('user', UserSchema);