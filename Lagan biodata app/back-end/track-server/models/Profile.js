const mongoose = require('mongoose');

const ProfileSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    name: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        enum: ['male', 'female'],
        required: true
    },
    images: {
        type: [String]
    },
    previouslyMarried: {
        type: Boolean,
        default: false
    },
    friends: {
        type: [mongoose.Schema.Types.ObjectId]
    },
    requestsSent: {
        type: [mongoose.Schema.Types.ObjectId]
    },
    requestsReceived: {
        type: [mongoose.Schema.Types.ObjectId]
    },
    blocked: {
        type: [mongoose.Schema.Types.ObjectId]
    },
    dateOfBirth: {
        type: Date,
        required: true
    },
    placeOfOrigin: {
        type: String,
        required: true
    },
    momVillage: {
        type: String
    },
    momsName: {
        type: String
    },
    dadsName: {
        type: String
    },
    currentCountryOfResidence: {
        type: String,
        required: true
    },
    currentPlaceOfResidence: {
        type: String,
    },
    citizenships: {
        type: String
    },
    education: {
        type: String
    },
    currentProfession: {
        type: String,
    },
    diet: {
        type: String
    },
    height: {
        type: Number
    },
    weight: {
        type: Number
    },
    horoscopeDetails: {
        type: String
    },
    likesAndInterest: {
        type: String
    },
    otherInformation: {
        type: String
    },

});

module.exports = Profile = mongoose.model('profile', ProfileSchema);